package org.cenario;

import org.elementos.ParedeQuebravel;

public class CenarioTeste {

	public static Cenario getCenarioTeste(){
		Cenario novo = new CenarioPadrao(7,7,20,20);
		MontadorCenario.adicionarBordas(novo);
		MontadorCenario.adicionarMeios(novo);
		novo.CasanoQuadrante(2, 1).adicionarElemento(new ParedeQuebravel(novo.CasanoQuadrante(2, 1)));
		novo.CasanoQuadrante(3, 2).adicionarElemento(new ParedeQuebravel(novo.CasanoQuadrante(3, 2)));
		novo.CasanoQuadrante(1, 5).adicionarElemento(new ParedeQuebravel(novo.CasanoQuadrante(1, 5)));
		return novo;
	}
	
}
