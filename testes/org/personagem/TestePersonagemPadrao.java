package org.personagem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.bomba.Bomba;
import org.desenho.PersonagemRenderer;
import org.elementos.TiposElementos;
import org.junit.Before;
import org.junit.Test;
import org.poder.ChutarBomba;
import org.poder.Poder;
import org.poder.PoderSegurar;
import org.poder.SegurarBomba;
import org.terreno.Coordenada;

public class TestePersonagemPadrao {

	Personagem P;
	
	@Before
	public void setUp() throws Exception {
		P = new PersonagemPadrao("Player") {
			
			@Override
			public PersonagemRenderer getRenderer() {
				return null;
			}
		};
		P.setCoordenada(new Coordenada(30,30));
	}

	@Test
	public void testAdicionarPoder() {
	}

	@Test
	public void testPoder1() {
		assertNull(P.getPoder1());
		Poder K = new ChutarBomba(P);
		P.adicionarPoder1(K);
		assertEquals(K,P.getPoder1());
	}

	@Test
	public void testPoder2() {
		assertNull(P.getPoder2());
		Poder K = new ChutarBomba(P);
		P.adicionarPoder2(K);
		assertEquals(K,P.getPoder2());
	}

	@Test
	public void testPoder3() {
		assertNull(P.getPoder3());
		Poder K = new ChutarBomba(P);
		P.adicionarPoder3(K);
		assertEquals(K,P.getPoder3());
	}

	@Test
	public void testPoderSegurar() {
		assertNull(P.getPoderSegurar());
		PoderSegurar K = new SegurarBomba(P);
		P.adicionarPoderSegurar(K);
		assertEquals(K,P.getPoderSegurar());
	}

	@Test
	public void testEliminar() {
		assertNull(P.eliminar());
	}

	@Test
	public void testGetBombasColocadas() {
		Bomba B1 = P.getBomba();
		Bomba B2 = P.getBomba();
		P.getBomba();
		P.adicionaBomba(B1);
		P.adicionaBomba(B2);
		Set<Bomba> BB = new HashSet<>();
		BB.add(B1);
		BB.add(B2);
		assertEquals(BB,new HashSet<>(P.getBombasColocadas()));
	}

	@Test
	public void testGetCoordenada() {
		assertEquals(new Coordenada(30,30), P.getCoordenada());
	}

	@Test
	public void testGetPosicionamento() {
		P.setPosicionamento(Posicionamento.BAIXO);
		assertEquals(Posicionamento.BAIXO, P.getPosicionamento());
		P.setPosicionamento(Posicionamento.ESQUERDA);
		assertEquals(Posicionamento.ESQUERDA, P.getPosicionamento());
		P.setPosicionamento(Posicionamento.PARADO);
		assertEquals(Posicionamento.PARADO, P.getPosicionamento());
		P.setPosicionamento(Posicionamento.CIMA);
		assertEquals(Posicionamento.CIMA, P.getPosicionamento());
	}

	@Test
	public void testGetUltimoPosicionamento() {
		P.setPosicionamento(Posicionamento.BAIXO);
		assertEquals(Posicionamento.BAIXO, P.getUltimoPosicionamento());
		P.setPosicionamento(Posicionamento.ESQUERDA);
		assertEquals(Posicionamento.ESQUERDA, P.getUltimoPosicionamento());
		P.setPosicionamento(Posicionamento.PARADO);
		assertEquals(Posicionamento.ESQUERDA, P.getUltimoPosicionamento());
		P.setPosicionamento(Posicionamento.CIMA);
		assertEquals(Posicionamento.CIMA, P.getUltimoPosicionamento());
	}

	@Test
	public void testNoBombas() {
		assertTrue(P.PossuiBombas());
		P.adicionaBomba(P.getBomba());
		assertFalse(P.PossuiBombas());
		P.incNoBombas(1);
		assertTrue(P.PossuiBombas());
	}

	@Test
	public void testPoderdeFogo() {
		assertEquals(2,P.getBomba().getPoder());
		P.incPoderdeFogo(2);
		assertEquals(4,P.getBomba().getPoder());
	}

	@Test
	public void testVelocidade() {
		assertEquals(5,P.getSpeed());
		P.incvelocidade(3);
		assertEquals(8,P.getSpeed());
	}

	@Test
	public void testPassa() {
		assertTrue(P.passa(TiposElementos.quadradoPoder));
		assertTrue(P.passa(TiposElementos.ovo));
		assertTrue(P.passa(TiposElementos.chao));
		assertFalse(P.passa(TiposElementos.bomba));
		assertFalse(P.passa(TiposElementos.paredeQuebravel));
		assertFalse(P.passa(TiposElementos.paredeInquebravel));
		assertTrue(P.passa(TiposElementos.fogo));
	}

	@Test
	public void testPossuiBombas() {
		assertTrue(P.PossuiBombas());
		P.adicionaBomba(P.getBomba());
		assertFalse(P.PossuiBombas());
	}

	@Test
	public void testSobrevive() {
		assertTrue(P.sobrevive(TiposElementos.quadradoPoder));
		assertTrue(P.sobrevive(TiposElementos.ovo));
		assertTrue(P.sobrevive(TiposElementos.chao));
		assertTrue(P.sobrevive(TiposElementos.bomba));
		assertFalse(P.sobrevive(TiposElementos.paredeQuebravel));
		assertFalse(P.sobrevive(TiposElementos.paredeInquebravel));
		assertFalse(P.sobrevive(TiposElementos.fogo));
	}

}
