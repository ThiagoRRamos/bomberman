package org.elemento;

import static org.junit.Assert.assertEquals;

import org.cenario.CenarioTeste;
import org.elementos.ElementoTerreno;
import org.elementos.Fogo;
import org.elementos.TiposElementos;
import org.junit.Test;
import org.terreno.Coordenada;

public class TesteElementosTerreno {

	@Test
	public void test() {
		ElementoTerreno ET = new Fogo(CenarioTeste.getCenarioTeste().CasaCorrespondente(new Coordenada(7,7)));
		assertEquals(TiposElementos.fogo,ET.getTipo());
	}

}
