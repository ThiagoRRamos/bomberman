package org.bomba;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.cenario.Cenario;
import org.cenario.CenarioTeste;
import org.junit.Before;
import org.junit.Test;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public class TesteBombaPrimaria {
	
	private Cenario C;
	
	@Before
	public void setUp() throws Exception {
		C = CenarioTeste.getCenarioTeste();
	}
	
	@Test
	public void testBombaCanto() {
		Bomba B = new BombaPrimaria(2,new Coordenada(30,30));
		Set<CelulaTerreno> NN = B.CelulasExplodidas(C);
		Set<CelulaTerreno> Ref = new HashSet<>();
		Ref.add(C.CasanoQuadrante(1, 1));
		Ref.add(C.CasanoQuadrante(2, 1));
		Ref.add(C.CasanoQuadrante(1, 2));
		Ref.add(C.CasanoQuadrante(1, 3));
		assertEquals(Ref,NN);
	}
	
	@Test
	public void testBomba2Paredes() {
		Bomba B = new BombaPrimaria(2,new Coordenada(70,30));
		Set<CelulaTerreno> NN = B.CelulasExplodidas(C);
		Set<CelulaTerreno> Ref = new HashSet<>();
		Ref.add(C.CasanoQuadrante(5, 1));
		Ref.add(C.CasanoQuadrante(2, 1));
		Ref.add(C.CasanoQuadrante(3, 1));
		Ref.add(C.CasanoQuadrante(3, 2));
		Ref.add(C.CasanoQuadrante(4, 1));
		assertEquals(Ref,NN);
	}
	
	@Test
	public void testBomba1PAredeInq() {
		Bomba B = new BombaPrimaria(2,new Coordenada(110,70));
		Set<CelulaTerreno> NN = B.CelulasExplodidas(C);
		Set<CelulaTerreno> Ref = new HashSet<>();
		Ref.add(C.CasanoQuadrante(5, 5));
		Ref.add(C.CasanoQuadrante(5, 4));
		Ref.add(C.CasanoQuadrante(5, 3));
		Ref.add(C.CasanoQuadrante(5, 2));
		Ref.add(C.CasanoQuadrante(5, 1));
		Ref.add(C.CasanoQuadrante(4, 3));
		Ref.add(C.CasanoQuadrante(3, 3));
		assertEquals(Ref,NN);
	}
	
	@Test
	public void testBomba1ParedeQ() {
		Bomba B = new BombaPrimaria(2,new Coordenada(70,70));
		Set<CelulaTerreno> NN = B.CelulasExplodidas(C);
		Set<CelulaTerreno> Ref = new HashSet<>();
		Ref.add(C.CasanoQuadrante(1, 3));
		Ref.add(C.CasanoQuadrante(2, 3));
		Ref.add(C.CasanoQuadrante(3, 3));
		Ref.add(C.CasanoQuadrante(4, 3));
		Ref.add(C.CasanoQuadrante(5, 3));
		Ref.add(C.CasanoQuadrante(3, 2));
		Ref.add(C.CasanoQuadrante(3, 4));
		Ref.add(C.CasanoQuadrante(3, 5));
		assertEquals(Ref,NN);
	}
	
	@Test
	public void testBombaPertoInq() {
		Bomba B = new BombaPrimaria(2,new Coordenada(70,110));
		Set<CelulaTerreno> NN = B.CelulasExplodidas(C);
		Set<CelulaTerreno> Ref = new HashSet<>();
		Ref.add(C.CasanoQuadrante(1, 5));
		Ref.add(C.CasanoQuadrante(2, 5));
		Ref.add(C.CasanoQuadrante(4, 5));
		Ref.add(C.CasanoQuadrante(5, 5));
		Ref.add(C.CasanoQuadrante(3, 5));
		Ref.add(C.CasanoQuadrante(3, 4));
		Ref.add(C.CasanoQuadrante(3, 3));
		assertEquals(Ref,NN);
	}

}
