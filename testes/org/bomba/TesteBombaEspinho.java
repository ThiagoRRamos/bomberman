package org.bomba;

import org.cenario.Cenario;
import org.cenario.CenarioPadrao;
import org.cenario.MontadorCenario;
import org.elementos.ParedeQuebravel;
import org.junit.Before;
import org.junit.Test;

public class TesteBombaEspinho {
	
	Cenario C;
	
	public Cenario getCenarioTeste(){
		Cenario novo = new CenarioPadrao(7,7,20,20);
		MontadorCenario.adicionarBordas(novo);
		MontadorCenario.adicionarMeios(novo);
		novo.CasanoQuadrante(2, 1).adicionarElemento(new ParedeQuebravel(novo.CasanoQuadrante(2, 1)));
		novo.CasanoQuadrante(3, 2).adicionarElemento(new ParedeQuebravel(novo.CasanoQuadrante(3, 2)));
		novo.CasanoQuadrante(1, 5).adicionarElemento(new ParedeQuebravel(novo.CasanoQuadrante(1, 5)));
		return novo;
	}

	@Before
	public void setUp() throws Exception {
		C = getCenarioTeste();
	}
	
	@Test
	public void testBombaCanto() {
	}

}
