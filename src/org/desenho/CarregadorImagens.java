package org.desenho;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

public class CarregadorImagens {

	public static CarregadorImagens instancia = new CarregadorImagens();
	public Map <String,BufferedImage> Imagens = new HashMap<>();
	
	public static void carregarImagem(String nomeArquivo){
		BufferedImage A = null;
		try {
			A =  ImageIO.read(new File(nomeArquivo));
			instancia.Imagens.put(nomeArquivo,A);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static BufferedImage pegarImagem(String nomeArquivo){
		if(!instancia.Imagens.containsKey(nomeArquivo))
			carregarImagem(nomeArquivo);
		return instancia.Imagens.get(nomeArquivo);
	}
	
}
