package org.desenho;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.personagem.Personagem;


public class PersonagemPikachuRenderer extends PersonagemRendererPadrao {

	public PersonagemPikachuRenderer(Personagem p) {
		P = p;
		numeroQuadros=3;
		Baixo = new BufferedImage[numeroQuadros];
		Cima = new BufferedImage[numeroQuadros];
		Direita = new BufferedImage[numeroQuadros];
		Esquerda = new BufferedImage[numeroQuadros];
		largura = 32;
		altura = 32;
		try{
			BufferedImage BIV = ImageIO.read(new File("Pikachu.png"));
			for(int i=0;i<2;i++){
				Baixo[i] = BIV.getSubimage(32*i+32, 0, 32, 32);
				Cima[i] = BIV.getSubimage(32*i+32, 96, 32, 32);
				Direita[i] = BIV.getSubimage(32*i+32, 64, 32, 32);
				Esquerda[i] = BIV.getSubimage(32*i+32, 32, 32, 32);
			}
			Baixo[2] = BIV.getSubimage(0, 0, 32, 32);
			Cima[2] = BIV.getSubimage(0, 96, 32, 32);
			Direita[2] = BIV.getSubimage(0, 64, 32, 32);
			Esquerda[2] = BIV.getSubimage(0, 32, 32, 32);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
