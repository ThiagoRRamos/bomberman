package org.desenho;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.personagem.Personagem;

public class PersonagemMarioRenderer extends PersonagemRendererPadrao{
	
	public PersonagemMarioRenderer(Personagem p) {
		P = p;
		numeroQuadros=8;
		Baixo = new BufferedImage[numeroQuadros];
		Cima = new BufferedImage[numeroQuadros];
		Direita = new BufferedImage[numeroQuadros];
		Esquerda = new BufferedImage[numeroQuadros];
		try{
			BufferedImage BIV = ImageIO.read(new File("MarioVertical.png"));
			BufferedImage BIL = ImageIO.read(new File("MarioLaterais.png"));
			for(int i=0;i<8;i++){
				Baixo[i] = BIV.getSubimage(20*i+8, 8, 20, 35);
				Cima[i] = BIV.getSubimage(173+20*i, 8, 20, 35);
				Direita[i] = BIL.getSubimage(20*i+8, 4, 20, 35);
				Esquerda[i] = BIL.getSubimage(170+20*i, 4, 20, 35);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
