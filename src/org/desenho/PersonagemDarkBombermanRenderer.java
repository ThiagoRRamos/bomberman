package org.desenho;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.personagem.Personagem;

public class PersonagemDarkBombermanRenderer extends PersonagemRendererPadrao {
	public PersonagemDarkBombermanRenderer(Personagem p){
		P = p;
		numeroQuadros=8;
		Baixo = new BufferedImage[numeroQuadros];
		Cima = new BufferedImage[numeroQuadros];
		Direita = new BufferedImage[numeroQuadros];
		Esquerda = new BufferedImage[numeroQuadros];
		AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
		tx.translate(-20, 0);
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		try{
			BufferedImage BIV = ImageIO.read(new File("BombermanMovimento.png"));
			for(int i=0;i<numeroQuadros;i++){
				Baixo[i] = BIV.getSubimage(77+21*i, 125, 21, 32);
				Cima[i] = BIV.getSubimage(75+24*i, 200, 23, 32);
				Esquerda[i] = BIV.getSubimage(77+23*i, 165, 21, 32);
				Direita[i] = op.filter(Esquerda[i], null);
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
