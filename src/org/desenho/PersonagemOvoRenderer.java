package org.desenho;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.personagem.Personagem;
import org.personagem.PersonagemOvo;

public class PersonagemOvoRenderer extends PersonagemRendererPadrao{
	
	PersonagemOvo PO;
	
	public PersonagemOvoRenderer(){
		
	}
	
	public PersonagemOvoRenderer(Personagem p) {
		P = p;
		PO = (PersonagemOvo) P;
		numeroQuadros=3;
		Baixo = new BufferedImage[numeroQuadros];
		Cima = new BufferedImage[numeroQuadros];
		Direita = new BufferedImage[numeroQuadros];
		Esquerda = new BufferedImage[numeroQuadros];
		try{
			BufferedImage BIV = ImageIO.read(new File("Cavalos.png"));
			for(int i=0;i<numeroQuadros;i++){
				Baixo[i] = BIV.getSubimage(29*i, 0, 26, 30);
				Cima[i] = BIV.getSubimage(29*i, 30, 26, 30);
				Direita[i] = BIV.getSubimage(32*i+82, 30, 32, 30);
				Esquerda[i] = BIV.getSubimage(32*i+82, 0, 32, 30);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	

}
