package org.desenho;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.personagem.Personagem;

public class PersonagemPatetaRenderer extends PersonagemRendererPadrao{
	
	public PersonagemPatetaRenderer(Personagem p) {
		P = p;
		numeroQuadros=7;
		Baixo = new BufferedImage[numeroQuadros];
		Cima = new BufferedImage[numeroQuadros];
		Direita = new BufferedImage[numeroQuadros];
		Esquerda = new BufferedImage[numeroQuadros];
		AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
		tx.translate(-18, 0);
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		try{
			BufferedImage BIV = ImageIO.read(new File("Goofy.png"));
			for(int i=0;i<numeroQuadros;i++){
				Baixo[i] = BIV.getSubimage(16*i, 0, 16, 40);
				Cima[i] = BIV.getSubimage(18*i, 40, 18, 40);
				Direita[i] = BIV.getSubimage(20*i, 80, 18, 40);
				Esquerda[i] = op.filter(Direita[i], null);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
