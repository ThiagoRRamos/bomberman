package org.desenho;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.personagem.Personagem;

public class PersonagemBombermanRenderer extends PersonagemRendererPadrao{
	public PersonagemBombermanRenderer(Personagem p){
		P = p;
		numeroQuadros=9;
		Baixo = new BufferedImage[numeroQuadros];
		Cima = new BufferedImage[numeroQuadros];
		Direita = new BufferedImage[numeroQuadros];
		Esquerda = new BufferedImage[numeroQuadros];
		AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
		tx.translate(-20, 0);
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		try{
			BufferedImage BIV = ImageIO.read(new File("BombermanMovimento.png"));
			Baixo[0] = BIV.getSubimage(45, 4, 21, 32);
			Cima[0] = BIV.getSubimage(45, 80, 23, 32);
			Esquerda[0] = BIV.getSubimage(45+23*i, 44, 21, 32);
			Direita[0] = op.filter(Esquerda[i], null);
			for(int i=1;i<numeroQuadros;i++){
				Baixo[i] = BIV.getSubimage(77+21*(i-1), 4, 21, 32);
				Cima[i] = BIV.getSubimage(75+24*(i-1), 80, 23, 32);
				Esquerda[i] = BIV.getSubimage(77+23*(i-1), 44, 21, 32);
				Direita[i] = op.filter(Esquerda[i], null);
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
