package org.desenho;

import java.awt.Graphics2D;
import java.util.List;

import org.cenario.Cenario;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;

public class CenarioRenderer implements Renderer {

	private Cenario cenario;

	public CenarioRenderer(Cenario cenario) {
		this.cenario = cenario;
	}

	@Override
	public void render(Graphics2D g) {
		for (List<CelulaTerreno> colunas : cenario.getCasas()) {
			for (CelulaTerreno cel : colunas) {
				cel.getRenderer().render(g);
			}
		}
		synchronized (cenario.getPersonagens()) {
			for (Personagem P : cenario.getPersonagens()) {
				P.getRenderer().render(g);
			}
		}
	}


}
