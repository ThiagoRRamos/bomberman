package org.desenho;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.elementos.Fogo;

public class FogoRenderer extends ElementoTerrenoRenderer {
	
	Fogo F;
	private BufferedImage A;
	
	public FogoRenderer(Fogo fogo) {
		F = fogo;
		
		BufferedImage BIV= CarregadorImagens.pegarImagem("fogo.png");
		A=BIV.getSubimage(26, 24, 20, 20);
	}

	@Override
	public void render(Graphics2D g) {
		g.drawImage(A,F.localizacao().getX(),F.localizacao().getY(),F.getCelulaAtual().getLargura(),F.getCelulaAtual().getAltura(),null);
		

	}

	

}
