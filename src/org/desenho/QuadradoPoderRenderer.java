package org.desenho;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.quadradopoder.QuadradoPoder;

public class QuadradoPoderRenderer extends ElementoTerrenoRenderer {
	
	private QuadradoPoder QP;
	private BufferedImage BI;
	
	public QuadradoPoderRenderer(QuadradoPoder Q,String FileName){
		QP = Q;
		BI = CarregadorImagens.pegarImagem(FileName);
	}
	
	@Override
	public void render(Graphics2D g) {
		g.drawImage(BI,QP.localizacao().getX(), QP.localizacao().getY(), QP.getCelulaAtual().getLargura(), QP.getCelulaAtual().getLargura(), null);
	}

}
