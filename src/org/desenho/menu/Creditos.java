package org.desenho.menu;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Creditos extends JPanel {

	private static final long serialVersionUID = 1L;
	
	public Creditos() {
		JLabel juanNome = new JLabel("Juan de Castro Pessoa");
		JLabel etNome = new JLabel("Thiago Ribeiro Ramos");
		JLabel assisNome = new JLabel("Lucas Amaral Cunha de Assis");
		JLabel tks = new JLabel("Obrigado por jogar!");
		
		JLabel pessoas = new JLabel();
		pessoas.setLayout(new GridLayout(3, 1));
		setLayout(new GridLayout(2, 1));
		
		pessoas.add(juanNome);
		pessoas.add(assisNome);
		pessoas.add(etNome);
		
		add(tks);
		add(pessoas);
	}
	

}
