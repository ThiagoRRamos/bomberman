package org.desenho.menu;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.desenho.CarregadorImagens;
import org.personagem.IdentificacaoPersonagem;

public class MenuPersonagem extends JPanel implements ActionListener{

	private static final long serialVersionUID = 1L;

	private static Map<String,IdentificacaoPersonagem> AssocId = new HashMap<>();
	private static Map<String,Image> AssocImagens = new HashMap<>();
	private List<JComboBox<String>> JJs = new ArrayList<>();
	static Image img1 = CarregadorImagens.pegarImagem("pateta.jpg").getScaledInstance(200, 200, Image.SCALE_DEFAULT);
	static Image img2 = CarregadorImagens.pegarImagem("super_mario.jpg").getScaledInstance(200, 200, Image.SCALE_DEFAULT);
	static Image img3 = CarregadorImagens.pegarImagem("bomberman.jpg").getScaledInstance(200, 200, Image.SCALE_DEFAULT);
	static Image img4 = CarregadorImagens.pegarImagem("black_bomberman.jpg").getScaledInstance(200, 200, Image.SCALE_DEFAULT);
	static Image img5 = CarregadorImagens.pegarImagem("pikachu.jpg").getScaledInstance(200, 200, Image.SCALE_DEFAULT);
	
	static {
		AssocId.put("White Bomberman", IdentificacaoPersonagem.whiteBomberman);
		AssocId.put("Black Bomberman", IdentificacaoPersonagem.blackBomberman);
		AssocId.put("Mario", IdentificacaoPersonagem.mario);
		AssocId.put("Pikachu", IdentificacaoPersonagem.pikachu);
		AssocId.put("Pateta", IdentificacaoPersonagem.pateta);
		AssocId.put("nenhum", IdentificacaoPersonagem.none);
		AssocImagens.put("White Bomberman", img3);
		AssocImagens.put("Black Bomberman", img4);
		AssocImagens.put("Mario", img2);
		AssocImagens.put("Pikachu", img5);
		AssocImagens.put("Pateta", img1);
		AssocImagens.put("nenhum", null);
	}
	
	private IdentificacaoPersonagem[] atual = new IdentificacaoPersonagem[4];
	JLabel[] picture = new JLabel[4];
	JButton OK;
	String[] nomesPersonagem = {"nenhum", "White Bomberman", "Black Bomberman", "Mario", "Pikachu", "Pateta" };
	
	{
		for(int i=0;i<4;i++){
			JJs.add(new JComboBox<String>(nomesPersonagem));
		}
	}

	private boolean selecionado;

	public MenuPersonagem(IdentificacaoPersonagem pers) {
		super(new GridLayout(1,4));
		this.atual[0] = pers;
		this.atual[1] = pers;
		this.atual[2] = pers;
		this.atual[3] = pers;

		OK =new JButton("OK");
		OK.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				selecionado = true;
			}
		});
		
		//Set up the picture.
		for(int i=0;i<4;i++){
			JPanel B = new JPanel(new BorderLayout());
			JComboBox<String> petList = JJs.get(i);
			petList.setSelectedIndex(0);
			petList.addActionListener(this);
			B.add(petList);
			picture[i] = new JLabel();
			JLabel picturea = picture[i];
			picturea.setFont(picturea.getFont().deriveFont(Font.ITALIC));
			picturea.setHorizontalAlignment(JLabel.CENTER);
			updateLabel(picturea,nomesPersonagem[petList.getSelectedIndex()]);
			picturea.setBorder(BorderFactory.createEmptyBorder(10,0,0,0));
			picturea.setPreferredSize(new Dimension(177, 122+10));
			B.add(petList, BorderLayout.PAGE_START);
			B.add(picturea, BorderLayout.CENTER);
			B.add(OK,BorderLayout.PAGE_END);
			B.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
			add(B);
		}

		//The preferred size is hard-coded to be the width of the
		//widest image and the height of the tallest image + the border.
		//A real program would compute this.

		//Lay out the demo.
	}


	/** Listens to the combo box. */
	public void actionPerformed(ActionEvent e) {
		JComboBox<String> cb = (JComboBox<String>) e.getSource();
		int qual = JJs.indexOf(cb);
		String petName = (String)cb.getSelectedItem();
		updateLabel(picture[qual],petName);
		atual[qual]=AssocId.get(petName);
	}

	public IdentificacaoPersonagem[] getIdPersonagem(){
		while(selecionado==false){
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return atual;
	}

	protected void updateLabel(JLabel a,String name) {
		if(AssocImagens.get(name)!=null){
			ImageIcon icon = new ImageIcon(AssocImagens.get(name));
			a.setIcon(icon);
			a.setToolTipText("A drawing of a " + name.toLowerCase());
		}
	}
}
