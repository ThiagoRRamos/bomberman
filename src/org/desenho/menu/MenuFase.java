package org.desenho.menu;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.fase.FabricaDeFabricaDeFase;
import org.fase.FabricadeFaseAbstrata;
import org.fase.IdentificacaoFase;

public class MenuFase extends JPanel implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	String[] petStrings = {"Escolha a Fase", "Primeira Fase", "Segunda Fase", "Fase Limpa"};
	Map<String,IdentificacaoFase> FaseCorrespondente = new HashMap<>();
	{
		FaseCorrespondente.put("Escolha a Fase", IdentificacaoFase.none);
		FaseCorrespondente.put("Primeira Fase", IdentificacaoFase.primeiraFase);
		FaseCorrespondente.put("Segunda Fase", IdentificacaoFase.segundaFase);
		FaseCorrespondente.put("Fase Limpa", IdentificacaoFase.faseLimpa);
	}

	IdentificacaoFase atual;
	JLabel picture;
	
	FabricadeFaseAbstrata FA;
	Image screen = new BufferedImage(420, 420,BufferedImage.TYPE_INT_RGB);
	Graphics2D AA = (Graphics2D) screen.getGraphics();

	private boolean ok;
	private JButton Selecionado;

	public MenuFase() {
		super(new BorderLayout());

		//Create the combo box, select the item at index 4.
		//Indices start at 0, so 4 specifies the pig.
		JComboBox petList = new JComboBox(petStrings);
		petList.setSelectedIndex(0);
		Selecionado = new JButton("OK");
		Selecionado.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ok=true;
			}
		});
		petList.addActionListener(this);

		//Set up the picture.
		picture = new JLabel();
		picture.setFont(picture.getFont().deriveFont(Font.ITALIC));
		picture.setHorizontalAlignment(JLabel.CENTER);
		updateLabel(petStrings[petList.getSelectedIndex()]);
		picture.setBorder(BorderFactory.createEmptyBorder(10,0,0,0));

		//The preferred size is hard-coded to be the width of the
		//widest image and the height of the tallest image + the border.
		//A real program would compute this.
		picture.setPreferredSize(new Dimension(200, 200));

		//Lay out the demo.
		add(petList, BorderLayout.PAGE_START);
		add(picture, BorderLayout.CENTER);
		add(Selecionado,BorderLayout.PAGE_END);
		setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
	}

	/** Listens to the combo box. */
	public void actionPerformed(ActionEvent e) {
		JComboBox cb = (JComboBox)e.getSource();
		String petName = (String)cb.getSelectedItem();
		atual = FaseCorrespondente.get(petName);
		updateLabel(petName);
	}

	protected void updateLabel(String name) {
		FA = new FabricaDeFabricaDeFase().fazerFase(atual);
		if(FA!=null)
			FA.getCenario().getRenderer().render(AA);
		ImageIcon icon = new ImageIcon(screen.getScaledInstance(300, 300, Image.SCALE_DEFAULT));
		picture.setIcon(icon);
		picture.setToolTipText("A drawing of a " + name.toLowerCase());
		picture.setText(null);
	}

	/** Returns an ImageIcon, or null if the path was invalid. */

	public IdentificacaoFase getIdFase(){
		while(ok==false){
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return atual;
	}
}
