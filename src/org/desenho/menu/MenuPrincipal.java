package org.desenho.menu;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.desenho.CarregadorImagens;
import org.principal.opcoes.Estado;
import org.principal.opcoes.Situacao;

public class MenuPrincipal extends JPanel {

	private static final long serialVersionUID = 1L;

	private JButton inicioButton;
	private JButton escolherPersonagem;
	private JButton sair;
	private JButton escolherFase;
	private Situacao situacao;

	

	public MenuPrincipal(Situacao sit) {
		this.situacao = sit;
		BufferedImage inicioImg = CarregadorImagens.pegarImagem("start-button.png");
		BufferedImage escolhaImg = CarregadorImagens.pegarImagem("choiceimg.png");
		BufferedImage exitImg = CarregadorImagens.pegarImagem("exit-sign.png");

		inicioButton = gerarBotao("Come�ar agora",inicioImg,"Start");
		escolherPersonagem = gerarBotao("Escolher Personagem", escolhaImg,"Escolher");
		escolherFase = new JButton("Escolher Fase");
		sair = gerarBotao("Sair", exitImg, "Sair");
		setLayout(new GridLayout(3, 1, 2, 3));
		configurarListener(inicioButton,Estado.jogando);
		configurarListener(escolherPersonagem,Estado.menuPersonagem);
		configurarListener(escolherFase,Estado.menuFase);
		configurarListener(sair,Estado.creditos);
		add(inicioButton);
		add(escolherPersonagem);
		add(escolherFase);
		add(sair);
	}

	private JButton gerarBotao(String texto, BufferedImage inicioImg,String legenda) {
		return new JButton(texto, new ImageIcon(
				inicioImg.getScaledInstance(100, 100, Image.SCALE_DEFAULT),
				legenda));
	}

	private void configurarListener(JButton Botao,final Estado creditos) {
		Botao.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					situacao.setSituacao(creditos);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
	}
	
	public void inicializar() {
		this.setVisible(true);
		this.desativaInicio();
		this.desativaPersonagem();
	}

	public void desativaInicio() {
		inicioButton.setEnabled(false);
	}

	public void ativaInicio() {
		inicioButton.setEnabled(true);
	}

	public void desativaPersonagem() {
		escolherPersonagem.setEnabled(false);
	}

	public void ativaPersonagem() {
		escolherPersonagem.setEnabled(true);
	}
	
	public void desativaMenuFase(){
		escolherFase.setEnabled(false);
	}
	
	public void ativaMenuFase(){
		escolherFase.setEnabled(true);
	}

}
