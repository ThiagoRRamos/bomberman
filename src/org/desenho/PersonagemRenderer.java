package org.desenho;

import java.awt.Graphics2D;



public interface PersonagemRenderer extends Renderer{

	public void renderizarXY(Graphics2D g,int x,int y);
}
