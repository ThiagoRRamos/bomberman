package org.desenho;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.personagem.Personagem;
import org.personagem.PersonagemOvo;

public class PersonagemOvoYoshiRenderer extends PersonagemOvoRenderer {

	public PersonagemOvoYoshiRenderer(){
		
	}
	
	public PersonagemOvoYoshiRenderer(Personagem p) {
		this();
		P = p;
		PO = (PersonagemOvo) P;
		numeroQuadros=5;
		Baixo = new BufferedImage[numeroQuadros];
		Cima = new BufferedImage[numeroQuadros];
		Direita = new BufferedImage[numeroQuadros];
		Esquerda = new BufferedImage[numeroQuadros];
		AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
		tx.translate(-30, 0);
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		try{
			BufferedImage BIV = ImageIO.read(new File("Yoshi2.png"));
			for(int i=0;i<numeroQuadros;i++){
				Baixo[i] = BIV.getSubimage(30*i, 40, 30, 40);
				Cima[i] = BIV.getSubimage(30*i, 80, 30, 40);
				Direita[i] = BIV.getSubimage(30*i, 0, 30, 40);
				Esquerda[i] = op.filter(Direita[i], null);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
