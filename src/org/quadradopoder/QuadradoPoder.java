package org.quadradopoder;

import org.elementos.ElementoTerrenoPadrao;
import org.elementos.TiposElementos;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;

public abstract class QuadradoPoder extends ElementoTerrenoPadrao {

	public QuadradoPoder(CelulaTerreno CT){
		super(CT);
		tipo = TiposElementos.quadradoPoder;
	}
	
	public QuadradoPoder(){
		super();
		tipo = TiposElementos.quadradoPoder;
	}
	
	public abstract  void aoAdicionar(Personagem P);
	public abstract  void aoRemover(Personagem P);

}
