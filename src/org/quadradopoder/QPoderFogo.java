package org.quadradopoder;

import org.desenho.QuadradoPoderRenderer;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;

public class QPoderFogo extends QuadradoPoder {

	
	public QPoderFogo(CelulaTerreno CT){
		super(CT);
		Renderizador = new QuadradoPoderRenderer(this,"Fireupsprite.png");
	}
	
	public QPoderFogo() {
		Renderizador = new QuadradoPoderRenderer(this,"Fireupsprite.png");
	}

	@Override
	public void aoAdicionar(Personagem P) {
		P.incPoderdeFogo(1);

	}

	@Override
	public void aoRemover(Personagem P) {
		P.incPoderdeFogo(-1);
	}

}
