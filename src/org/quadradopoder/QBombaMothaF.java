package org.quadradopoder;

import org.bomba.FabricaBombaMothaF;
import org.desenho.QuadradoPoderRenderer;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;

public class QBombaMothaF extends QuadradoPoder {

	private FabricaBombaMothaF FE;
	
	public QBombaMothaF(CelulaTerreno casanoQuadrante) {
		super(casanoQuadrante);
		Renderizador = new QuadradoPoderRenderer(this,"PowerBomb.png");
	}

	@Override
	public void aoAdicionar(Personagem P) {
		FE = new FabricaBombaMothaF();
		P.adicionarFabricadeBombas(FE);
		
	}

	@Override
	public void aoRemover(Personagem P) {
		P.removerFabricadeBombas(FE);
	}

}
