package org.quadradopoder;

import org.desenho.QuadradoPoderRenderer;
import org.personagem.Personagem;
import org.poder.ChutarBomba;
import org.terreno.CelulaTerreno;

public class QChutarBomba extends QuadradoPoder {

	private ChutarBomba K;
	
	public QChutarBomba(CelulaTerreno CT){
		super(CT);
		Renderizador = new QuadradoPoderRenderer(this,"Kicksprite.png");
	}
	
	public QChutarBomba() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void aoAdicionar(Personagem P) {
		K = new ChutarBomba(P);
		P.adicionarPoder1(K);
		
	}

	@Override
	public void aoRemover(Personagem P) {
		P.removerPoder1(K);
	}

}
