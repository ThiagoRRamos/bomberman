package org.quadradopoder;

import org.bomba.FabricaEspinho;
import org.desenho.QuadradoPoderRenderer;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;

public class QBombaEspinho extends QuadradoPoder {

	private FabricaEspinho FE;
	
	public QBombaEspinho(CelulaTerreno casanoQuadrante) {
		super(casanoQuadrante);
		Renderizador = new QuadradoPoderRenderer(this,"Piercebombsprite.png");
	}

	@Override
	public void aoAdicionar(Personagem P) {
		FE = new FabricaEspinho();
		P.adicionarFabricadeBombas(FE);
		
	}

	@Override
	public void aoRemover(Personagem P) {
		P.removerFabricadeBombas(FE);
	}

}
