package org.quadradopoder;

import org.desenho.QuadradoPoderRenderer;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;

public class QPoderVelocidade extends QuadradoPoder {

	public QPoderVelocidade(CelulaTerreno CT){
		super(CT);
		Renderizador = new QuadradoPoderRenderer(this,"Skatesprite.png");
	}
	
	public QPoderVelocidade() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void aoAdicionar(Personagem P) {
		P.incvelocidade(5);

	}

	@Override
	public void aoRemover(Personagem P) {
		P.incvelocidade(-5);
	}

}
