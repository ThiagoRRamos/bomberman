package org.quadradopoder;

import org.desenho.QuadradoPoderRenderer;
import org.personagem.Personagem;
import org.poder.EmpurrarBomba;
import org.terreno.CelulaTerreno;

public class QEmpurrarBomba extends QuadradoPoder {

	private EmpurrarBomba K;
	{
		Renderizador = new QuadradoPoderRenderer(this,"Boxing_Glove.png");
	}
	
	public QEmpurrarBomba(CelulaTerreno CT){
		super(CT);
	}
	
	public QEmpurrarBomba() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void aoAdicionar(Personagem P) {
		K = new EmpurrarBomba(P);
		P.adicionarPoder1(K);
		
	}

	@Override
	public void aoRemover(Personagem P) {
		P.removerPoder1(K);
	}

}
