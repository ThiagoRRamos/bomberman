package org.bomba;

import org.personagem.Personagem;
import org.terreno.Coordenada;


public class BombaControleRemoto extends BombaPrimaria {

	public BombaControleRemoto(int poder, Coordenada c) {
		super(poder,c);
	}

	public BombaControleRemoto(int poderdeFogo, Coordenada clone,
			Personagem personagemPadrao) {
		super(poderdeFogo,clone,personagemPadrao);
	}

	@Override
	public boolean explodeporTempo() {
		return false;
	}

	@Override
	public int TempoExplosao() {
		return 0;
	}

}
