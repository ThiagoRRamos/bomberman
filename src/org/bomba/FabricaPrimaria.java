package org.bomba;

import org.personagem.Personagem;
import org.terreno.Coordenada;

public class FabricaPrimaria extends FabricadeBombas {

	@Override
	public Bomba getBomba(int poder,Coordenada C) {
		return new BombaPrimaria(poder,C);
	}

	@Override
	public Bomba getBomba(int poderdeFogo, Coordenada clone,
			Personagem personagemPadrao) {
		return new BombaPrimaria(poderdeFogo,clone,personagemPadrao);
	}

}
