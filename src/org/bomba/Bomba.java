package org.bomba;

import java.util.Set;

import org.cenario.Cenario;
import org.elementos.ElementoTerreno;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public interface Bomba extends ElementoTerreno{
		
	public Set<CelulaTerreno> CelulasExplodidas(Cenario C);
	public boolean explodeporTempo();
	public int TempoExplosao();
	public Coordenada localizacao();
	public Personagem lancador();
	public int getPoder();
	public void setCoordenadas(Coordenada nova);
	public void setExplodida(boolean b);
	public boolean foiExplodida();

}