package org.bomba;

import org.elementos.TiposElementos;
import org.personagem.Personagem;
import org.terreno.Coordenada;

public class BombaEspinho extends BombaPrimaria {

	protected boolean atravessaEstado(TiposElementos estado) {
		return estado!=TiposElementos.paredeInquebravel ;
	}
	
	public BombaEspinho(int poder,Coordenada C){
		super(poder,C);
	}
	
	public BombaEspinho(int poder, Coordenada C,Personagem P) {
		super(poder,C,P);
	}

}
