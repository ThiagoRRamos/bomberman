package org.terreno;

public class Coordenada {
	
	private int x;
	private int y;

	public Coordenada(int i, int j) {
		x=i;
		y=j;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public void inc(int nx,int ny){
		x += nx;
		y += ny;
	}
	
	public Coordenada clone(){
		return new Coordenada(x,y);
	}

	@Override
	public boolean equals(Object obj) {
		Coordenada C=null;
		try{
			C = (Coordenada) obj;
		}catch(ClassCastException E){
			return false;
		}
		return (x==C.getX() && y==C.getY());
	}
}
