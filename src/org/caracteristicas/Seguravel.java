package org.caracteristicas;

import org.terreno.CelulaTerreno;

public interface Seguravel {
	
	public void onSegurado();
	public void aoSerSolto(CelulaTerreno cT);
}
