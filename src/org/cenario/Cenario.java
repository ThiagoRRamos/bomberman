package org.cenario;

import java.util.Collection;
import java.util.List;

import org.bomba.Bomba;
import org.desenho.CenarioRenderer;
import org.mecanica.MecanicaJogo;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public interface Cenario {
	
	public void adicionarPersonagem(Personagem novo);
	public CelulaTerreno CasaCorrespondente(Coordenada C);
	public CelulaTerreno CasanoQuadrante(int x, int y);
	public CelulaTerreno CelulaAcima(CelulaTerreno CT);
	public CelulaTerreno CelulaAbaixo(CelulaTerreno CT);
	public CelulaTerreno CelulaDireita(CelulaTerreno CT);
	public CelulaTerreno CelulaEsquerda(CelulaTerreno CT);
	boolean celulaPertencea(CelulaTerreno celulaTerreno, Collection<Coordenada> L);
	public List<CelulaTerreno> CelulasMesmaColuna(CelulaTerreno CT);
	public List<CelulaTerreno> CelulasMesmaLinha(CelulaTerreno CT);
	public List<CelulaTerreno> CelulasVizinhas(CelulaTerreno CT);
	public int distancia(CelulaTerreno C1,CelulaTerreno C2);
	public void explodirBomba(Bomba b, int i);
	public int getAlturadeUmaCasa();
	public int getAlturaemCasas();
	public int getAlturaTotal();
	public List<List<CelulaTerreno>> getCasas();
	public List<CelulaTerreno> getChaos();
	public Collection<CelulaTerreno> getEspeciaisDescobertos();
	public int getLarguradeUmaCasa();
	public int getLarguraemCasas();
	public int getLarguraTotal();
	public List<Personagem> getPersonagens();
	public void moverBomba(Bomba topo, CelulaTerreno origem, CelulaTerreno destino);
	public void removerPersonagem(Personagem p);
	public void setMecanicaJogo(MecanicaJogo M);
	public CenarioRenderer getRenderer();

}
