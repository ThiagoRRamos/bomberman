package org.cenario;

import org.elementos.ElementoTerreno;
import org.elementos.ParedeInquebravel;
import org.terreno.CelulaTerreno;

public class MontadorCenario {
	
	public static void adicionarBordas(Cenario novo) {
		int maxX = novo.getLarguraemCasas()-1;
		int maxY = novo.getAlturaemCasas()-1;
		novo.CasanoQuadrante(0, 0).adicionarElemento(new ParedeInquebravel(novo.CasanoQuadrante(0, 0)));
		novo.CasanoQuadrante(maxX, 0).adicionarElemento(new ParedeInquebravel(novo.CasanoQuadrante(maxX, 0)));
		novo.CasanoQuadrante(0, maxY).adicionarElemento(new ParedeInquebravel(novo.CasanoQuadrante(0, maxY)));
		novo.CasanoQuadrante(maxX, maxY).adicionarElemento(new ParedeInquebravel(novo.CasanoQuadrante(maxX, maxY)));
		for(int i=1;i<maxX;i++){
			novo.CasanoQuadrante(i, 0).adicionarElemento(new ParedeInquebravel(novo.CasanoQuadrante(i, 0)));
			novo.CasanoQuadrante(i, maxY).adicionarElemento(new ParedeInquebravel(novo.CasanoQuadrante(i, maxY)));
		}
		for(int i=1;i<maxY;i++){
			novo.CasanoQuadrante(0, i).adicionarElemento(new ParedeInquebravel(novo.CasanoQuadrante(0, i)));
			novo.CasanoQuadrante(maxX, i).adicionarElemento(new ParedeInquebravel(novo.CasanoQuadrante(maxX, i)));
		}
	}

	public static void adicionarMeios(Cenario novo) {
		int maxX = novo.getLarguraemCasas();
		int maxY = novo.getLarguraemCasas();
		for(int i=1;i<maxX;i++){
			if(i%2==0){
				for(int j=1;j<maxY;j++){
					if(j%2==0){
						novo.CasanoQuadrante(i, j).adicionarElemento(new ParedeInquebravel(novo.CasanoQuadrante(i, j)));
					}
				}
			}
		}
	}
	
	public static void adicionarElemento(Cenario C, ElementoTerreno ET, int coluna,int linha){
		adicionarElemento(ET,C.CasanoQuadrante(coluna, linha));
	}
	
	public static void adicionarElemento(ElementoTerreno ET,CelulaTerreno CT){
		ET.inicializarLocalizacao(CT);
		CT.adicionarElemento(ET);
	}
}
