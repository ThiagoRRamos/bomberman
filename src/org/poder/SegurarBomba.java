package org.poder;

import org.bomba.Bomba;
import org.caracteristicas.Seguravel;
import org.elementos.TiposElementos;
import org.mecanica.MecanicaJogo;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;

public class SegurarBomba implements PoderSegurar {
	
	private Personagem dono;
	
	public SegurarBomba(Personagem p) {
		dono = p;
	}

	@Override
	public Seguravel iniciar(MecanicaJogo M) {
		CelulaTerreno CT = M.getCenario().CasaCorrespondente(dono.getCoordenada());
		if(CT.getEstado()==TiposElementos.bomba){
			dono.segurar(CT.tirarTopo());
			((Bomba) dono.getSegurado()).setCoordenadas(dono.getCoordenada());
		}
		return dono.getSegurado();
	}
	
	@Override
	public void finalizar(MecanicaJogo M) {
		CelulaTerreno CT = M.getCenario().CasaCorrespondente(dono.getCoordenada());
		dono.soltar(CT);
	}

}
