package org.poder;

import org.bomba.Bomba;
import org.cenario.Cenario;
import org.elementos.TiposElementos;
import org.mecanica.MecanicaJogo;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;

public class EmpurrarBomba implements Poder {

	private Personagem Dono;
	
	public EmpurrarBomba(Personagem p) {
		Dono = p;
	}

	@Override
	public void executar(MecanicaJogo M) {
		Cenario C = M.getCenario();
		CelulaTerreno La = null;
		CelulaTerreno Aqui = C.CasaCorrespondente(Dono.getCoordenada());
		switch(Dono.getUltimoPosicionamento()){
			case CIMA: 
				La = C.CelulaAcima(Aqui);
				if(La.getEstado()==TiposElementos.bomba)
					C.moverBomba((Bomba) La.getTopo(),La,C.CelulaAcima(La));
			case ESQUERDA:
				La = C.CelulaEsquerda(Aqui);
				if(La.getEstado()==TiposElementos.bomba)
					C.moverBomba((Bomba) La.getTopo(),La,C.CelulaEsquerda(La));
			case DIREITA:
				La = C.CelulaDireita(Aqui);
				if(La.getEstado()==TiposElementos.bomba)
					C.moverBomba((Bomba) La.getTopo(),La,C.CelulaDireita(La));
			case BAIXO:
				La = C.CelulaAbaixo(Aqui);
				if(La.getEstado()==TiposElementos.bomba)
					C.moverBomba((Bomba) La.getTopo(),La,C.CelulaAbaixo(La));
		}
		
	}

}
