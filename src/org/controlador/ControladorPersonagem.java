package org.controlador;

import org.cenario.Cenario;
import org.mecanica.MecanicaJogo;
import org.personagem.Personagem;

public interface ControladorPersonagem {
	
	public Personagem getPersonagem();
	public void PersonagemgoBaixo();
	public void PersonagemgoCima();
	public void PersonagemgoDireita();
	public void PersonagemgoEsquerda();
	public void PersonagemLancar();
	public void PersonagemPoder1();
	public void PersonagemPoder2();
	public void PersonagemSegurar();
	public void PersonagemSoltarBomba();
	public void setCenario(Cenario fase);
	public void setMecanicaJogo(MecanicaJogo MecJ);
	public void setPersonagem(Personagem P);

}