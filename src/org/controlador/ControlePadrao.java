package org.controlador;

import org.bomba.Bomba;
import org.cenario.Cenario;
import org.elementos.TiposElementos;
import org.mecanica.MecanicaJogo;
import org.personagem.Personagem;
import org.personagem.Posicionamento;
import org.poder.Poder;
import org.poder.PoderSegurar;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public class ControlePadrao implements ControladorPersonagem{

	protected Personagem Person;
	protected Cenario Local;
	protected MecanicaJogo MJ;

	public ControlePadrao() {
		super();
	}

	public void PersonagemPoder2() {
		Poder P = Person.getPoder2();
		if(P!=null)
			P.executar(MJ);
	}
	
	public void PersonagemPoder3() {
		Poder P = Person.getPoder3();
		if(P!=null)
			P.executar(MJ);
	}

	public void PersonagemSoltarBomba() {
		CelulaTerreno CT = Local.CasaCorrespondente(Person.getCoordenada());
		if(CT.getEstado()==TiposElementos.chao){
			if(Person.PossuiBombas()){
				Bomba B = Person.getBomba();
				Person.adicionaBomba(B);
				if(B.explodeporTempo()){
					MJ.adicionarBomba(B, B.TempoExplosao());
				}
				CT.adicionarElemento(B);
			}
		}
	}

	public void PersonagemPoder1() {
		Poder P = Person.getPoder1();
		if(P!=null)
			P.executar(MJ);
	}

	public void PersonagemSegurar() {
		PoderSegurar PS = Person.getPoderSegurar();
		if(PS!=null){
			PS.iniciar(MJ);
		}
	}

	public void PersonagemLancar() {
		PoderSegurar PS = Person.getPoderSegurar();
		if(PS!=null)
			PS.finalizar(MJ);
	}

	@Override
	public void setPersonagem(Personagem P) {
		Person = P;
	}

	@Override
	public void PersonagemgoCima() {
		Person.setPosicionamento(Posicionamento.CIMA);
	}

	@Override
	public void PersonagemgoBaixo() {
		Person.setPosicionamento(Posicionamento.BAIXO);
		
	}

	@Override
	public void PersonagemgoEsquerda() {
		Person.setPosicionamento(Posicionamento.ESQUERDA);
	}

	@Override
	public void PersonagemgoDireita() {
		Person.setPosicionamento(Posicionamento.DIREITA);
	}

	public boolean PersonagemPodeir(Coordenada Cantiga, Coordenada Cnova) {
		CelulaTerreno C1 = MJ.getCenario().CasaCorrespondente(Cantiga);
		CelulaTerreno C2 = MJ.getCenario().CasaCorrespondente(Cnova);
		if(C1==C2||Person.passa(C2.getEstado()))
			return true;
		return false;
	}

	@Override
	public Personagem getPersonagem() {
		return Person;
	}

	public void setMecanicaJogo(MecanicaJogo MecJ) {
		MJ = MecJ;
	}

	@Override
	public void setCenario(Cenario fase) {
		Local = fase;
	}

}