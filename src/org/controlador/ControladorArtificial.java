package org.controlador;

import java.util.ArrayList;
import java.util.List;

import org.inteligenciaartificial.BigAIBrain;
import org.personagem.Posicionamento;
import org.terreno.CelulaTerreno;

public class ControladorArtificial extends ControlePadrao{
	
	private BigAIBrain B;
	
	public ControladorArtificial(BigAIBrain B){
		this.B=B;
		B.adicionarControladorDependente(this);
	}
	
	
	public void movimentar(){
		if(!Person.estaMorto())
			synchronized (B) {
				CelulaTerreno Aqui = Local.CasaCorrespondente(Person.getCoordenada());
				Float ValorMax = Float.NEGATIVE_INFINITY;
				CelulaTerreno Destino = null;
				int possiveis = 0;
				for(CelulaTerreno CT : Local.CelulasVizinhas(Aqui)){
					if(Person.passa(CT.getEstado())&&Person.sobrevive(CT.getEstado())){
						possiveis++;
						if(B.getValor(CT)>ValorMax){
							ValorMax = B.getValor(CT);
							Destino = CT;
						}
					}
				}
				if(Destino!=null&&ValorMax>B.getValor(Aqui)){
					irParaDestino(Destino);
				}else{
					soltarBomba();
					executarPoder();
					parar();
				}
			}
	}


	private void executarPoder() {
		// TODO Auto-generated method stub
		
	}


	private void soltarBomba() {
		CelulaTerreno Aqui = Local.CasaCorrespondente(Person.getCoordenada());
		int possiveis = VizinhosDisponiveis().size();
		if(possiveis==1){
			if(Math.random()>0.9&&B.getValor(Aqui)>=-0.01)
				PersonagemSoltarBomba();
		}else if(possiveis==2){
			if(Math.random()>0.95&&B.getValor(Aqui)>=1)
				PersonagemSoltarBomba();
		}else{
			if(Math.random()>0.99&&B.getValor(Aqui)>=0)
				PersonagemSoltarBomba();
		}
	}
	
	private void parar() {
		Person.setPosicionamento(Posicionamento.PARADO);
	}
	
	private List<CelulaTerreno> VizinhosDisponiveis(){
		ArrayList<CelulaTerreno> CTs = new ArrayList<>();
		for(CelulaTerreno CT : Local.CelulasVizinhas(Local.CasaCorrespondente(Person.getCoordenada()))){
			if(Person.passa(CT.getEstado())&&Person.sobrevive(CT.getEstado())){
				CTs.add(CT);
			}
		}
		return CTs;
	}


	private void irParaDestino(CelulaTerreno destino) {
		CelulaTerreno Aqui = Local.CasaCorrespondente(Person.getCoordenada());
		if(destino==Local.CelulaAcima(Aqui)){
			PersonagemgoCima();
		}
		if(destino==Local.CelulaAbaixo(Aqui)){
			PersonagemgoBaixo();
		}
		if(destino==Local.CelulaEsquerda(Aqui)){
			PersonagemgoEsquerda();
		}
		if(destino==Local.CelulaDireita(Aqui)){
			PersonagemgoDireita();
		}
	}

}
