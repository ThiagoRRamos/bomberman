package org.personagem;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import org.bomba.Bomba;
import org.bomba.FabricaPrimaria;
import org.bomba.FabricadeBombas;
import org.caracteristicas.Seguravel;
import org.elementos.TiposElementos;
import org.poder.Poder;
import org.poder.PoderSegurar;
import org.quadradopoder.QuadradoPoder;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public abstract class PersonagemPadrao implements Personagem {
	
	protected List<Bomba> BombasColocadas;
	protected Posicionamento Dir;
	protected Seguravel elementoSegurado;
	protected Stack<FabricadeBombas> FBs;
	protected Coordenada Localizacao;
	protected String Nome;
	protected int NumeroBombas;
	protected List<TiposElementos> Passa;
	protected int PoderdeFogo;
	protected Stack<Poder> Poderes1 = new Stack<>();
	protected Stack<Poder> Poderes2 = new Stack<>();
	protected Stack<Poder> Poderes3 = new Stack<>();
	protected List<QuadradoPoder> Quadrados = new ArrayList<>();
	protected PoderSegurar PoderS;
	protected List<TiposElementos> Sobrevive;
	protected Posicionamento ultimoPosicionamento;
	protected int velocidade;
	protected boolean morto=false;
	
	public PersonagemPadrao(String nome){
		Dir = Posicionamento.PARADO;
		ultimoPosicionamento = Posicionamento.BAIXO;
		FBs = new Stack<>();
		FBs.push(new FabricaPrimaria());
		PoderdeFogo = 2;
		NumeroBombas = 1;
		velocidade = 5;
		Nome = nome;
		BombasColocadas = new LinkedList<Bomba>();
		Passa = new ArrayList<TiposElementos>();
		Sobrevive = new ArrayList<TiposElementos>();
		Passa.add(TiposElementos.chao);
		Passa.add(TiposElementos.fogo);
		Passa.add(TiposElementos.quadradoPoder);
		Passa.add(TiposElementos.ovo);
		Sobrevive.add(TiposElementos.chao);
		Sobrevive.add(TiposElementos.quadradoPoder);
		Sobrevive.add(TiposElementos.bomba);
		Sobrevive.add(TiposElementos.ovo);
	}
	
	public void adicionaBomba(Bomba b){
		BombasColocadas.add(b);
	}
	
	@Override
	public void adicionarEstadoAndavel(TiposElementos paredequebravel) {
		Passa.add(paredequebravel);
		Sobrevive.add(paredequebravel);
	}
	
	@Override
	public void adicionarFabricadeBombas(FabricadeBombas FB){
		FBs.push(FB);
	}
	
	public void adicionarPoder(QuadradoPoder topo){
		Quadrados.add(topo);
	}
	
	@Override
	public void adicionarPoder1(Poder k) {
		Poderes1.push(k);
	}
	
	@Override
	public void adicionarPoder2(Poder k) {
		Poderes2.push(k);
	}
	
	@Override
	public void adicionarPoder3(Poder k) {
		Poderes3.push(k);
	}

	@Override
	public void adicionarPoderSegurar(PoderSegurar sB) {
		PoderS = sB;		
	}

	@Override
	public boolean eliminado() {
		return morto;
	}
	
	@Override
	public Personagem eliminar(){
		morto = true;
		return null;
	}

	@Override
	public boolean estaMorto() {
		return morto;
	}

	@Override
	public Bomba getBomba(){
		return FBs.peek().getBomba(PoderdeFogo, getCoordenada().clone(),this);
	}
	
	@Override
	public List<Bomba> getBombasColocadas() {
		return BombasColocadas;
	}
	
	@Override
	public Coordenada getCoordenada(){
		return Localizacao;
	}
	
	public String getNome(){
		return Nome;
	}
	
	@Override
	public Poder getPoder1() {
		if(!Poderes1.isEmpty())
			return Poderes1.peek();
		return null;
	}
	
	@Override
	public Poder getPoder2() {
		if(!Poderes2.isEmpty())
			return Poderes2.peek();
		return null;
	}
	
	@Override
	public Poder getPoder3() {
		if(!Poderes3.isEmpty())
			return Poderes3.peek();
		return null;
	}
	
	@Override
	public PoderSegurar getPoderSegurar() {
		return PoderS;
	}
	
	@Override
	public Posicionamento getPosicionamento() {
		return Dir;
	}
	
	@Override
	public Seguravel getSegurado() {
		return elementoSegurado;
	}

	@Override
	public int getSpeed(){
		return velocidade;
	}
	
	@Override
	public Posicionamento getUltimoPosicionamento() {
		return ultimoPosicionamento;
	}
	
	public void incNoBombas(int valor){
		NumeroBombas+=valor;
	}
	
	public void incPoderdeFogo(int valor){
		PoderdeFogo+=valor;
	}
	
	@Override
	public void incvelocidade(int valor) {
		velocidade+=valor;
	}
	
	@Override
	public boolean passa(TiposElementos estado) {
		return Passa.contains(estado);
	}
	
	@Override
	public boolean PossuiBombas(){	
		return (NumeroBombas>BombasColocadas.size());
	}
	
	public void removerBombadaLista(Bomba B){
		BombasColocadas.remove(B);
	}

	@Override
	public void removerEstadoAndavel(TiposElementos paredequebravel) {
		Passa.remove(paredequebravel);
		Sobrevive.remove(paredequebravel);
	}

	@Override
	public void removerFabricadeBombas(FabricadeBombas FB){
		FBs.remove(FB);
	}

	@Override
	public void removerPoder1(Poder k) {
		Poderes1.remove(k);
	}
	
	@Override
	public void removerPoder2(Poder k) {
		Poderes2.remove(k);
	}

	@Override
	public void removerPoder3(Poder k) {
		Poderes3.remove(k);
	}
	
	@Override
	public void removerPoderSegurar(PoderSegurar sB) {
		if(PoderS==sB)
			PoderS = null;
	}
	
	@Override
	public void segurar(Seguravel tirarTopo) {
		elementoSegurado = tirarTopo;
	}
	
	@Override
	public void setCoordenada(Coordenada Novo){
		Localizacao = Novo;
	}
	
	@Override
	public void setPosicionamento(Posicionamento P){
		Dir = P;
		if(P!=Posicionamento.PARADO){
			ultimoPosicionamento = P;
		}
	}
	
	@Override
	public boolean sobrevive(TiposElementos estado) {
		return Sobrevive.contains(estado);
	}
	
	@Override
	public void soltar(CelulaTerreno cT) {
		if(elementoSegurado!=null)
			elementoSegurado.aoSerSolto(cT);
		elementoSegurado = null;
	}
}
