package org.personagem;

import org.desenho.PersonagemOvoYoshiRenderer;
import org.poder.Poder;
import org.poder.PoderYoshiEngolir;


public class PersonagemOvoYoshi extends PersonagemOvo {
	
	Poder P2 = new PoderYoshiEngolir(this);
	
	public PersonagemOvoYoshi(Personagem Dentro) {
		super(Dentro);
		Renderizador = new PersonagemOvoYoshiRenderer(this);
	}
	
	@Override
	public Poder getPoder2() {
		return P2;
	}
}