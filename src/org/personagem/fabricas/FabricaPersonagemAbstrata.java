package org.personagem.fabricas;

import org.personagem.IdentificacaoPersonagem;
import org.personagem.Personagem;

public abstract class FabricaPersonagemAbstrata {
	
	public abstract Personagem criaPersonagem(IdentificacaoPersonagem tipo, String playerName);

}
