package org.personagem;

import org.desenho.PersonagemPikachuRenderer;
import org.desenho.PersonagemRendererPadrao;

public class PersonagemPikachu extends PersonagemPadrao {

	private PersonagemRendererPadrao Renderizador = new PersonagemPikachuRenderer(this);
	
	public PersonagemPikachu(String nome) {
		super(nome);
	}

	@Override
	public PersonagemRendererPadrao getRenderer() {
		return Renderizador;
	}

}
