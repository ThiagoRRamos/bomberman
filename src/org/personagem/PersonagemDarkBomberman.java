package org.personagem;


import org.desenho.PersonagemDarkBombermanRenderer;
import org.desenho.PersonagemRendererPadrao;

public class PersonagemDarkBomberman extends PersonagemPadrao {
private PersonagemRendererPadrao Renderizador = new PersonagemDarkBombermanRenderer(this);
	
	public PersonagemDarkBomberman(String nome) {
		super(nome);
	}
	@Override
	public PersonagemRendererPadrao getRenderer() {
		return Renderizador;
	}
}
