package org.principal;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import org.mecanica.MecanicaJogo;

public class Jogo extends JPanel implements Runnable {
	private static final long serialVersionUID = 1L;

	private MecanicaJogo mec;
	BufferedImage screen;
	Graphics2D graph;

	public Jogo(MecanicaJogo mec) {
		this.mec = mec;
		Dimension d = mec.getDimensoesCenario();
		screen = new BufferedImage((int) d.getWidth(), (int) d.getHeight(),
				BufferedImage.TYPE_INT_RGB);
		graph = (Graphics2D) screen.getGraphics();
		graph.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
	}

	private void desenharInformacoeslaterais(MecanicaJogo mec2) {
	}

	@Override
	public void paint(Graphics g) {
			mec.render(graph);
			desenharInformacoeslaterais(mec);
			g.drawImage(screen, 30, 20, null);
		if(mec.FimdeJogo()){
			Font font = new Font("Arial", Font.PLAIN, 30);
			g.setColor(Color.WHITE);
			g.setFont(font);
			g.drawString(mec.getVencedor().getNome()+" venceu!", 100, 100);
		}
	}

	@Override
	public void run() {
		while (true) {
			repaint();
			try {
				Thread.sleep(25);
			} catch (InterruptedException e) {
			}
		}
	}

}
