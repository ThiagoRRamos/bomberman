package org.principal;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.controlador.ControlTeclado;
import org.controlador.ControladorArtificial;
import org.controlador.ControlePadrao;
import org.desenho.menu.Creditos;
import org.desenho.menu.MenuFase;
import org.desenho.menu.MenuPersonagem;
import org.desenho.menu.MenuPrincipal;
import org.fase.FabricaDeFabricaDeFase;
import org.fase.FabricadeFaseAbstrata;
import org.fase.IdentificacaoFase;
import org.inteligenciaartificial.BigAIBrain;
import org.mecanica.MecanicaJogo;
import org.personagem.IdentificacaoPersonagem;
import org.personagem.fabricas.FabricaPersonagem;
import org.principal.opcoes.Estado;
import org.principal.opcoes.Situacao;

public class Principal {

	/**
	 * @param args
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */

	public static MecanicaJogo MJ;
	public static BigAIBrain BBB;
	public static Jogo game;
	public static MenuPersonagem menuPers;
	public static MenuPrincipal menu;
	public static Creditos cred;
	public static MenuFase menuFase;
	public static JPanel container;
	
	public static void main(String[] args) throws InterruptedException,
			ExecutionException {
		
		final ExecutorService ES = Executors.newCachedThreadPool();
		Situacao situacao = new Situacao(Estado.menuPrincipal);
		ControlTeclado CT = new ControlTeclado();
		List<JPanel> Paineis = new ArrayList<>();
		
		inicializarPaineis(situacao);
		
		Paineis.add(menuPers);
		Paineis.add(menuFase);
		Paineis.add(cred);
		Paineis.add(menu);

		JFrame topFrame = criaFrame(ES);
		topFrame.add(container);
		topFrame.pack();
		topFrame.setSize(new Dimension(500, 500));
		topFrame.addKeyListener(CT);
		topFrame.setVisible(true);

		Estado antigo = situacao.getSituacao();
		while (true) {
			Thread.sleep(50);
			while (!situacao.mudou()) {
				if(situacao.getSituacao()==(Estado.jogando) && MJ.FimdeJogo()){
					situacao.setSituacao(Estado.creditos);
					Thread.sleep(2000);
				}
				if (antigo!=(situacao.getSituacao())) {
					situacao.setMudou(true);
					antigo = situacao.getSituacao();
					switch (situacao.getSituacao()) {
					case menuPrincipal:
						mudarfocovisivel(menu, Paineis);
						break;
					case jogando:
						game = new Jogo(MJ);
						Paineis.add(game);
						game.addKeyListener(CT);
						container.add(game);
						ES.submit(MJ);
						ES.submit(game);
						mudarfocovisivel(game, Paineis);
						break;
					case menuPersonagem:
						mudarfocovisivel(menuPers, Paineis);
						break;
					case menuFase:
						mudarfocovisivel(menuFase, Paineis);
						break;
					case creditos:
						mudarfocovisivel(cred, Paineis);
						ES.shutdownNow();
					default:
						break;
					}
				}
			}
			if (situacao.mudou()) {
				atualizarFrame(topFrame);
				situacao.setMudou(false);
			}
			if (situacao.getSituacao()==(Estado.menuPersonagem)) {
				IdentificacaoPersonagem[] Ids = menuPers.getIdPersonagem();
				menu.desativaPersonagem();
				situacao.setJaEscolheuPersonagem(true);
				if(situacao.jaEscolheuFase() && situacao.jaEscolheuPersonagem())
					menu.ativaInicio();
				configurarPersonagens(CT, Ids);
				situacao.setSituacao(Estado.menuPrincipal);
				situacao.setMudou(true);
			}
			if(situacao.getSituacao()==(Estado.menuFase)){
				situacao.setJaEscolheuFase(true);
				if(situacao.jaEscolheuFase() && situacao.jaEscolheuPersonagem())
					menu.ativaInicio();
				menu.desativaMenuFase();
				menu.ativaPersonagem();
				MJ = new MecanicaJogo(definirFase(menuFase.getIdFase()), ES);
				BBB = new BigAIBrain(MJ);
				situacao.setMudou(true);
				situacao.setSituacao(Estado.menuPrincipal);
			}
			
		}

	}

	private static void inicializarPaineis(Situacao situacao) {
		menu = new MenuPrincipal(situacao);
		menu.inicializar();
		
		cred = new Creditos();
		cred.setVisible(false);
		
		menuPers = new MenuPersonagem(IdentificacaoPersonagem.none);
		menuPers.setVisible(false);
		
		menuFase = new MenuFase();
		menuFase.setVisible(false);

		container = new JPanel();
		container.setLayout(new CardLayout(2, 2));
		container.add(menu);
		container.add(menuPers);
		container.add(cred);
		container.add(menuFase);
	}

	private static void atualizarFrame(JFrame topFrame) {
		topFrame.pack();
		topFrame.revalidate();
		topFrame.setSize(new Dimension(500, 500));
		topFrame.setVisible(true);
	}

	private static void configurarPersonagens(ControlTeclado humano, IdentificacaoPersonagem[] Ids) {
		ControladorArtificial CA = new ControladorArtificial(BBB);
		ControladorArtificial CB = new ControladorArtificial(BBB);
		ControladorArtificial CC = new ControladorArtificial(BBB);
		definirPersonagem(Ids[0], humano);
		definirPersonagem(Ids[1], CA);
		definirPersonagem(Ids[2], CB);
		definirPersonagem(Ids[3], CC);
		MJ.adicionarControle(humano);
		MJ.adicionarControle(CA);
		MJ.adicionarControle(CB);
		MJ.adicionarControle(CC);
	}

	private static void mudarfocovisivel(JPanel menu, List<JPanel> Paineis) {
		for(JPanel P:Paineis){
			if(P==menu)
				P.setVisible(true);
			else
				P.setVisible(false);
		}
		menu.requestFocus();
	}

	private static FabricadeFaseAbstrata definirFase(IdentificacaoFase tipoFase) {
		FabricaDeFabricaDeFase fabfab = new FabricaDeFabricaDeFase();
		FabricadeFaseAbstrata fab = fabfab.fazerFase(tipoFase);
		return fab;
		
	}

	private static void definirPersonagem(IdentificacaoPersonagem esc,
			ControlePadrao ctrl) {
		FabricaPersonagem factory = new FabricaPersonagem();
		ctrl.setPersonagem(factory.criaPersonagem(esc,"Player 1"));
	}

	private static JFrame criaFrame(final ExecutorService exec) {
		JFrame f = new JFrame("Bomberman");
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				exec.shutdownNow();
			}

			@Override
			public void windowIconified(WindowEvent e) {
			}
		});
		return f;
	}

}
