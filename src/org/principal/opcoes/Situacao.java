package org.principal.opcoes;

public class Situacao {
	
	private Estado atual;
	private boolean JaEscolheuPersonagem;
	private boolean JaEscolheuFase;
	private boolean mudou;
	
	public Situacao(Estado situacao) {
		atual = situacao;
		mudou = false;
		JaEscolheuFase = false;
		JaEscolheuPersonagem = false;
	}
	
	public void setSituacao(Estado situacao){
		atual = situacao;
	}
	
	public Estado getSituacao(){
		return atual;
	}

	public boolean jaEscolheuPersonagem() {
		return JaEscolheuPersonagem;
	}

	public void setJaEscolheuPersonagem(boolean jaEscolheuPersonagem) {
		JaEscolheuPersonagem = jaEscolheuPersonagem;
	}

	public boolean jaEscolheuFase() {
		return JaEscolheuFase;
	}

	public void setJaEscolheuFase(boolean jaEscolheuFase) {
		JaEscolheuFase = jaEscolheuFase;
	}
	
	@Override
	public Situacao clone(){
		Situacao Nova = new Situacao(this.getSituacao());
		Nova.setJaEscolheuFase(this.jaEscolheuFase());
		Nova.setJaEscolheuPersonagem(this.jaEscolheuPersonagem());
		return Nova;
	}

	public boolean mudou() {
		return mudou;
	}

	public void setMudou(boolean mudou) {
		this.mudou = mudou;
	}
	
}

