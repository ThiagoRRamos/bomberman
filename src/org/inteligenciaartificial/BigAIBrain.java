package org.inteligenciaartificial;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bomba.Bomba;
import org.cenario.Cenario;
import org.controlador.ControladorArtificial;
import org.elementos.TiposElementos;
import org.mecanica.MecanicaJogo;
import org.terreno.CelulaTerreno;

public class BigAIBrain implements Runnable{
	
	private Map<CelulaTerreno,Float> Difusao = new HashMap<>();
	private Cenario Cen;
	private MecanicaJogo Mec;
	private List<ControladorArtificial> Controladores = new ArrayList<>();
	
	public BigAIBrain(){
		
	}
	
	public BigAIBrain(MecanicaJogo M){
		Mec = M;
		Cen = M.getCenario();
		M.setBBB(this);
	}
	
	public float getValor(CelulaTerreno CT){
		if(Difusao==null || !Difusao.containsKey(CT))
			return 0f;
		return Difusao.get(CT);
	}
	
	public void RodadaDifusao(){
		synchronized (this){
			Map<CelulaTerreno,Float> NovaDifusao = new HashMap<>();
			for(List<CelulaTerreno> LCT : Cen.getCasas()){
				for(CelulaTerreno CT : LCT){
					synchronized (CT) {
						switch(CT.getEstado()){
						case fogo:
							Difusao.put(CT, -10f);
							NovaDifusao.put(CT, -10f);
							break;
						case paredeInquebravel:
							Difusao.put(CT, 0f);
							NovaDifusao.put(CT, 0f);
							break;
						case paredeQuebravel :
							Difusao.put(CT, 0f);
							NovaDifusao.put(CT, 0f);
							break;
						case quadradoPoder :
							Difusao.put(CT, 8f);
							NovaDifusao.put(CT, 8f);
							break;
						case bomba :
							if(CT.getTopo()!=null){
								Difusao.put(CT, (float) -2*((Bomba) CT.getTopo()).getPoder());
							}
							NovaDifusao.put(CT, -8f);
							break;
						case ovo :
							Difusao.put(CT, 10f);
							NovaDifusao.put(CT, 10f);
							break;
						}
					}
				}
			}
			for(List<CelulaTerreno> LCT : Cen.getCasas()){
				for(CelulaTerreno CT : LCT){
					if(CT.getEstado()==TiposElementos.chao){
						Float valor = Difusao.get(CT);
						valor = valor/2;
						for(CelulaTerreno V : Cen.CelulasVizinhas(CT)){
							valor+=Difusao.get(V)/8;
						}
						NovaDifusao.put(CT,valor);
					}
				}
			}
			Difusao = NovaDifusao;
		}
	}

	public Map<CelulaTerreno, Float> getDifusao() {
		return Difusao;
	}

	@Override
	public void run() {
		try{
			inicializerDifusao();
			while(!Mec.FimdeJogo()){
				RodadaDifusao();
				for(ControladorArtificial C : Controladores){
					C.movimentar();
				}
				try {
					Thread.sleep(40);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}catch(Exception E){
			E.printStackTrace();
		}
		
	}

	private void inicializerDifusao() {
		for(List<CelulaTerreno> LCT : Cen.getCasas()){
			for(CelulaTerreno CT : LCT){
				Difusao.put(CT, 0f);
			}
		}
	}

	public void adicionarControladorDependente(ControladorArtificial controladorArtificial) {
		synchronized (Controladores) {
			Controladores.add(controladorArtificial);
		}
	}
	
}


	
