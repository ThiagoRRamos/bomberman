package org.ovos;

import org.desenho.OvoRenderer;
import org.personagem.Personagem;
import org.personagem.PersonagemOvo;
import org.terreno.CelulaTerreno;

public class OvoBasico extends Ovo {

	public OvoBasico(CelulaTerreno CT) {
		super(CT);
		Renderizador = new OvoRenderer(this);
	}

	@Override
	public Personagem gerarPersonagem(Personagem p) {
		return new PersonagemOvo(p);
	}

	@Override
	public void onSegurado() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aoSerSolto(CelulaTerreno cT) {
		cT.adicionarElemento(this);
		this.setCelulaAtual(cT);
		this.setCoordenadas(cT.getCoordenadaCorrespondente());
	}

}
