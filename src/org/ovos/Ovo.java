package org.ovos;

import org.desenho.ElementoTerrenoRenderer;
import org.desenho.OvoRenderer;
import org.elementos.ElementoTerreno;
import org.elementos.TiposElementos;
import org.personagem.Personagem;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public abstract class Ovo implements ElementoTerreno {
	
	protected Coordenada Local;
	protected OvoRenderer Renderizador;
	protected CelulaTerreno Celula;
	
	@Override
	public ElementoTerrenoRenderer getRenderer(){
		return Renderizador;
	}
	
	public Ovo(CelulaTerreno CT){
		Local = CT.getCoordenadaCorrespondente();
		Celula = CT;
	}
	
	@Override
	public Coordenada localizacao() {
		return Local;
	}
	
	@Override
	public TiposElementos getTipo() {
		return TiposElementos.ovo;
	}
	
	@Override
	public void setCoordenadas(Coordenada nova) {
		Local = nova;
	}
	
	@Override
	public void setCelulaAtual(CelulaTerreno CT) {
		Celula = CT;
	}

	@Override
	public CelulaTerreno getCelulaAtual() {
		return Celula;
	}
	
	@Override
	public void inicializarLocalizacao(CelulaTerreno CT) {
		setCelulaAtual(CT);
		setCoordenadas(CT.getCoordenadaCorrespondente());
	}

	public abstract Personagem gerarPersonagem(Personagem p);

}
