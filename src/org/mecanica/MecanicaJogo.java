package org.mecanica;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import org.bomba.Bomba;
import org.cenario.Cenario;
import org.controlador.ControladorPersonagem;
import org.elementos.Fogo;
import org.elementos.TiposElementos;
import org.fase.FabricadeFaseAbstrata;
import org.inteligenciaartificial.BigAIBrain;
import org.ovos.Ovo;
import org.personagem.Personagem;
import org.quadradopoder.QuadradoPoder;
import org.terreno.CelulaTerreno;

public class MecanicaJogo implements Runnable{
	
	private Cenario Fase;
	private List<ControladorPersonagem> Controles;
	private ExplosorApagador EB;
	private ExecutorService ES;
	private Map<Personagem,Long> UltimaMorte;
	private Movimentador Mover;
	private FabricadeFaseAbstrata FA;
	private BigAIBrain BBB;
	
	public MecanicaJogo(FabricadeFaseAbstrata F,ExecutorService ES){
		FA = F;
		Fase = FA.getCenario();
		Fase.setMecanicaJogo(this);
		Controles = new ArrayList<ControladorPersonagem>();
		this.ES = ES;
		EB = new ExplosorApagador(this);
		UltimaMorte = new HashMap<>();
		
	}
	
	public void adicionarBomba(Bomba B, long tempo){
			EB.adicionarBomba(B, tempo);	
	}
	
	public void adicionarFogo(Fogo F,CelulaTerreno CT, long tempo){
		EB.adicionarFogo(F,CT,tempo);	
	}
	
	public void MatarPersonagens(){
		synchronized (Fase.getPersonagens()) {
			List<Personagem> Morredores = new ArrayList<>();
			for(Personagem P : Fase.getPersonagens()){
				CelulaTerreno CT = Fase.CasaCorrespondente(P.getCoordenada());
				if(!P.sobrevive(CT.getEstado()) && podeMorrer(P)){
					Morredores.add(P);
				}
			}
			for(Personagem P : Morredores){
				eliminarPersonagem(P);
			}
		}
	}
	
	public boolean podeMorrer(Personagem P){
		if(!UltimaMorte.containsKey(P)){
			return true;
		}
		long now = System.currentTimeMillis();
		boolean ok = (now-UltimaMorte.get(P))>2000;
		return ok;
	}

	private void eliminarPersonagem(Personagem P) {
		Personagem Novo = P.eliminar();
		ControladorPersonagem CP = ControleCorrespondente(P);
		if(Novo!=null){
			trocarPersonagens(P, Novo);
			UltimaMorte.put(Novo, System.currentTimeMillis());
		}else{
			Fase.removerPersonagem(P);
			Controles.remove(CP);
		}
	}
	
	public void Round(){
		MatarPersonagens();
		atribuirPoderes();
	}
	
	private void atribuirPoderes() {
		Set<Personagem> ModificadosOvo = new LinkedHashSet<>();
		for(Personagem P : Fase.getPersonagens()){
			CelulaTerreno CT = Fase.CasaCorrespondente(P.getCoordenada());
			if(CT.getEstado()==TiposElementos.quadradoPoder){
				atribuirPoder(P,(QuadradoPoder) CT.getTopo());
				CT.removerElemento(CT.getTopo());
			}
			if(CT.getEstado()==TiposElementos.ovo){
				ModificadosOvo.add(P);
			}
		}
		for(Personagem P : ModificadosOvo){
			CelulaTerreno CT = Fase.CasaCorrespondente(P.getCoordenada());
			synchronized (CT) {
				atribuirOvo(P,(Ovo) CT.getTopo());
				CT.removerElemento(CT.getTopo());
			}
		}
	}

	private void atribuirOvo(Personagem p, Ovo topo) {
		if(topo!=null){
			Personagem K = topo.gerarPersonagem(p);
			trocarPersonagens(p, K);
		} 
	}

	private void trocarPersonagens(Personagem Antigo, Personagem Novo) {
		ControladorPersonagem C = ControleCorrespondente(Antigo);
		C.setPersonagem(Novo);
		synchronized (Fase.getPersonagens()) {
			Fase.getPersonagens().remove(Antigo);
			Fase.getPersonagens().add(Novo);
		}
	}

	private void atribuirPoder(Personagem p, QuadradoPoder topo) {
		p.adicionarPoder(topo);
		topo.aoAdicionar(p);
	}

	public boolean FimdeJogo(){
		return (Controles.size()==1);
	}

	@Override
	public void run() {
		try{
			iniciar();
			while(!FimdeJogo()){
				Round();
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				}
			}
		}catch(Exception E){
			E.printStackTrace();
		}
	}

	private void iniciar() {
		Mover = new Movimentador(this);
		ES.submit(EB);
		ES.submit(Mover);
		ES.submit(BBB);
		for(Personagem P : Fase.getPersonagens()){
			FA.inicializarPersonagem(P);
		}
	}

	public void adicionarControle(ControladorPersonagem cT) {
		Controles.add(cT);
		cT.setMecanicaJogo(this);
		cT.setCenario(Fase);
		cT.getPersonagem().setCoordenada(FA.getPosicoesInicio().get(Fase.getPersonagens().size()));
		Fase.adicionarPersonagem(cT.getPersonagem());
		
	}
	
	public ControladorPersonagem ControleCorrespondente(Personagem P){
		for(ControladorPersonagem CP : Controles){
			if(CP.getPersonagem()==P){
				return CP;
			}
		}
		return null;
	}
	
	public void render(Graphics2D g){
		Fase.getRenderer().render(g);
	}

	public Cenario getCenario() {
		return Fase;
	}
	
	public Dimension getDimensoesCenario(){
		return new Dimension(Fase.getLarguraTotal(), Fase.getAlturaTotal());
	}

	public void pararBomba(Bomba topo) {
		topo.lancador().removerBombadaLista(topo);
		EB.removerBomba(topo);
	}

	public Personagem getVencedor() {
		for(Personagem P : Fase.getPersonagens())
			if(!P.eliminado())
				return P;
		return null;
	}

	public BigAIBrain getBBB() {
		return BBB;
	}

	public void setBBB(BigAIBrain bBB) {
		BBB = bBB;
	}
}
