package org.mecanica;

import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.bomba.Bomba;
import org.elementos.Fogo;
import org.terreno.CelulaTerreno;

public class ExplosorApagador implements Runnable {
	
	MecanicaJogo Mec;
	private List<ParBombaTempo> Bombas;
	private List<TrincaFogoLocalTempo> Fogos;
	
	class TrincaFogoLocalTempo {

		private Fogo F;
		private CelulaTerreno C;
		private long t;

		public TrincaFogoLocalTempo(Fogo f, CelulaTerreno cT, long tempo) {
			F = f;
			C = cT;
			t = tempo;
		}

	}
	
	class ParBombaTempo{
		public Bomba B;
		public long TempodeExplosao;
		
		public ParBombaTempo(Bomba B,long l){
			this.B = B;
			TempodeExplosao = l;
		}
	}
	
	
	public ExplosorApagador(MecanicaJogo mecanicaJogo) {
		Mec= mecanicaJogo;
		Bombas = new LinkedList<>();
		Fogos = new LinkedList<>();
	}

	@Override
	public void run() {
		while(!Mec.FimdeJogo()){
			try{
				explodirBombasporTempo();
				apagarFogosporTempo();
			}catch(Exception E){
				E.printStackTrace();
			}
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}
		}
	}
	
	public void explodirBombasporTempo(){
		synchronized (Bombas) {
			long agora = Calendar.getInstance().getTimeInMillis();
			Set<ParBombaTempo> PPPs = new HashSet<ParBombaTempo>();
			for(ParBombaTempo PBT : Bombas){
				if(PBT.TempodeExplosao < agora){
					if(!PBT.B.foiExplodida())
						Mec.getCenario().explodirBomba(PBT.B,1500);
					PPPs.add(PBT);
				} else { 
					break;
				}
			}
			Bombas.removeAll(PPPs);	
		}
	}
	
	private void apagarFogosporTempo() {
		synchronized (Fogos) {
			long agora = Calendar.getInstance().getTimeInMillis();
			Set<TrincaFogoLocalTempo> PPPs = new HashSet<>();
			for(TrincaFogoLocalTempo PBT : Fogos){
				if(PBT.t < agora){
					PBT.C.apagarFogo(PBT.F);
					PPPs.add(PBT);
				} else { 
					break;
				}
			}
			Fogos.removeAll(PPPs);
		}
	}

	public void adicionarFogo(Fogo f, CelulaTerreno cT, long tempo) {
		synchronized (Fogos) {
			Fogos.add(new TrincaFogoLocalTempo(f,cT,tempo));
		}
	}

	public void adicionarBomba(Bomba b, long tempo) {
		synchronized (Bombas) {
			Bombas.add(new ParBombaTempo(b, System.currentTimeMillis()+tempo));
		}
		
	}

	public void removerBomba(Bomba topo) {
		synchronized (Bombas) {
			ParBombaTempo T = null;
			for(ParBombaTempo BBBB : Bombas){
				if(BBBB.B==topo){
					T = BBBB;
					break;
				}
			}
			Bombas.remove(T);
		}
	}

}
