package org.elementos;

import org.desenho.ParedeQuebravelRenderer;
import org.terreno.CelulaTerreno;

public class ParedeQuebravel extends ElementoTerrenoPadrao  {
	
	public ParedeQuebravel(CelulaTerreno CT){
		super(CT);
		Renderizador = new ParedeQuebravelRenderer(this);
		tipo = TiposElementos.paredeQuebravel;
	}

}
