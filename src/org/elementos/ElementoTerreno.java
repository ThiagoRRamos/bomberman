package org.elementos;

import org.caracteristicas.Seguravel;
import org.desenho.ElementoTerrenoRenderer;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public interface ElementoTerreno extends Seguravel{

	public TiposElementos getTipo();
	public Coordenada localizacao();
	public void setCoordenadas(Coordenada nova);
	public ElementoTerrenoRenderer getRenderer();
	public void setCelulaAtual(CelulaTerreno CT);
	public CelulaTerreno getCelulaAtual();
	public void inicializarLocalizacao(CelulaTerreno CT);

}
