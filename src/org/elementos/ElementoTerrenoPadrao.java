package org.elementos;

import org.desenho.ElementoTerrenoRenderer;
import org.terreno.CelulaTerreno;
import org.terreno.Coordenada;

public abstract class ElementoTerrenoPadrao implements ElementoTerreno {

	protected TiposElementos tipo;
	protected Coordenada C;
	protected ElementoTerrenoRenderer Renderizador;
	protected CelulaTerreno CT;
	
	public ElementoTerrenoRenderer getRenderer(){
		return Renderizador;
	}
	
	public ElementoTerrenoPadrao(CelulaTerreno Celula){
		C = Celula.getCoordenadaCorrespondente();
		CT = Celula;
	}
	
	public ElementoTerrenoPadrao() {
		// TODO Auto-generated constructor stub
	}
	
	public void setCoordenadas(Coordenada nova){
		C = nova;
	}

	@Override
	public TiposElementos getTipo() {
		return tipo;
	}
	
	public Coordenada localizacao(){
		return C;
	}
	
	@Override
	public void setCelulaAtual(CelulaTerreno CT) {
		this.CT = CT;
		
	}

	@Override
	public CelulaTerreno getCelulaAtual() {
		return CT;
	}
	
	@Override
	public void onSegurado() {
		// TODO Auto-generated method stub
	}

	@Override
	public void aoSerSolto(CelulaTerreno cT) {
		cT.adicionarElemento(this);
	}
	
	@Override
	public void inicializarLocalizacao(CelulaTerreno CT) {
		setCelulaAtual(CT);
		setCoordenadas(CT.getCoordenadaCorrespondente());
	
	}

}
