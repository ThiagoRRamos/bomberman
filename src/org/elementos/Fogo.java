package org.elementos;

import org.desenho.FogoRenderer;
import org.terreno.CelulaTerreno;

public class Fogo extends ElementoTerrenoPadrao {
	
	public Fogo(CelulaTerreno CT){
		super(CT);
		Renderizador = new FogoRenderer(this);
		tipo = TiposElementos.fogo;
	}

	public Fogo() {
		// TODO Auto-generated constructor stub
	}

}
